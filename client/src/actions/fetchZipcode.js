import axios from 'axios'
import { FETCHED_ZIPCODE } from './types'

const fetchZipcode = () => async dispatch => {
  if ('geolocation' in navigator) {
    const pos = await getCurrentPosition({ enableHighAccuracy: true, timeout: 10000, maximumAge: 0 })
    const latlng = `${pos.coords.latitude},${pos.coords.longitude}`
    const res = await axios.get('/api/place/zipcode?latlng=' + latlng)
    dispatch({ type: FETCHED_ZIPCODE, payload: res.data})
  } else {
    dispatch({ type: FETCHED_ZIPCODE, payload: null})
  }
}

// Helper function, turn navigator into promise
const getCurrentPosition = (options = {}) => {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(resolve, reject, options)
  })
}

export default fetchZipcode