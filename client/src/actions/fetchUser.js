import axios from 'axios'
import { FETCH_USER, FETCHED_USER } from './types'

const fetchUser = () => async dispatch => {
  dispatch({ type: FETCH_USER })
  try {
    const res = await axios.get('/api/current_user')
    dispatch({ type: FETCHED_USER, payload: res.data })
  } catch (e) {
    const error = { error: e.response }
    dispatch({ type: FETCHED_USER, payload: error })
  }
}

export default fetchUser