import axios from 'axios'
import { UPDATE_RATING } from './types'

const ratePlace = (score, placeid) => async dispatch => {
  const res = await axios.post('/api/rate',
    { score, placeid },
    {
      headers: { 'Content-Type': 'application/json' },
    }
  )
  dispatch({ type: UPDATE_RATING, payload: res.data})
}

export default ratePlace