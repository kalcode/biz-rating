// USER
export const FETCH_USER = '@auth/fetch_user'
export const FETCHED_USER = '@auth/fetched_user'

// PLACES
export const FETCH_PLACES = '@places/fetch_places'
export const FETCHED_PLACES = '@places/fetched_places'

// PLACES Details
export const FETCH_DETAILS = '@details/fetch_details'
export const FETCHED_DETAILS = '@details/fetched_details'

// Update rating
export const UPDATE_RATING = '@details/UPDATE_RATING'

// Get current location
export const FETCHED_ZIPCODE = '@zipcode/fetched_zipcode'