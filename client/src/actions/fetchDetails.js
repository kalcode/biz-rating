import axios from 'axios'
import { FETCH_DETAILS, FETCHED_DETAILS } from './types'

const fetchDetails = (placeid) => async dispatch => {
  dispatch({ type: FETCH_DETAILS })
  let res = null 
  try {
    res = await axios.get('/api/place/details?placeid=' + placeid)
  } catch (error) {
    res = { data: { error } }
  }
  dispatch({ type: FETCHED_DETAILS, payload: res.data})
}

export default fetchDetails