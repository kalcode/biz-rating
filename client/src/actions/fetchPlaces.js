import axios from 'axios'
import { FETCH_PLACES, FETCHED_PLACES } from './types'

const fetchPlaces = (zipcode) => async dispatch => {
  dispatch({ type: FETCH_PLACES })
  try {
    const res = await axios.get('/api/place/search?zipcode=' + zipcode)
    dispatch({ type: FETCHED_PLACES, payload: res.data})
  } catch (error) {
    dispatch({ type: FETCHED_PLACES, payload: { error: error }})
  }
}

export default fetchPlaces