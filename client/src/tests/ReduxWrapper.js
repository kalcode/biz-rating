
import React from 'react'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import reduxThunk from 'redux-thunk'
import reducers from '../reducers'


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducers,
  {},
  composeEnhancers(
    applyMiddleware(reduxThunk)
  )
)

const ReduxWrapper = ({ children, mockStore }) => {
    return (
        <Provider store={mockStore || store}>
            {children}
        </Provider>
    )
}

export default ReduxWrapper