import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../actions'
import * as types from '../actions/types'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
 
const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
 var mock = new MockAdapter(axios);

describe('async actions', () => {
  afterEach(() => {
    mock.reset()
  })
 
  it('creates FETCHED_USER when fetching user has been done', () => {
    const body = {
      "_id": "5ad0c32ce85d120014a5fb9d",
      "googleId": "106086648844371109059",
      "__v": 0,
    }

    mock
      .onGet('/api/current_user').reply(200, body)
 
 
    const expectedActions = [
      { type: types.FETCH_USER },
      { type: types.FETCHED_USER, payload: body },
    ]
    const store = mockStore({ auth: {} })
 
    return store.dispatch(actions.fetchUser()).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})