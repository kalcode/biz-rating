import React from 'react';
import ReactDOM from 'react-dom';
import App from '../components/App';

import ReduxWrapper from './ReduxWrapper'

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ReduxWrapper><App /></ReduxWrapper>, div);
  ReactDOM.unmountComponentAtNode(div);
});
