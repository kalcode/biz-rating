import React from 'react';
import Landing from '../pages/Landing';
import { shallow, render } from 'enzyme'

it('renders without crashing', () => {
  shallow(<Landing />);
});


it('page first tag is main', () => {
  const wrapper = shallow(<Landing />);
  expect(wrapper.type()).toEqual('main')
});