import React from 'react';

import ReduxWrapper from './ReduxWrapper'
import SignIn from '../components/SignIn'
import { mount } from 'enzyme';
import configureMockStore from 'redux-mock-store'

import { MemoryRouter as Router } from 'react-router-dom';

const mountWithRouter = node => mount(<Router>{node}</Router>)
const mockStore = configureMockStore()

it('SignIn: "Sign with Google" text when auth is null', () => {
    const store = mockStore({ auth: null })
    const wrapper = mount(<ReduxWrapper mockStore={store}><SignIn /></ReduxWrapper>);
    expect(wrapper.find('SignIn').text()).toBe('Sign in with Google')
})


it('SignIn: "Go To Rate Some Biz!" text when auth is valid', () => {
    const store = mockStore({ auth: {} })
    const wrapper = mountWithRouter(<ReduxWrapper mockStore={store}><SignIn /></ReduxWrapper>);
    expect(wrapper.find('SignIn').text()).toBe('Go To Rate Some Biz!')
})
