import React,  { Component } from 'react'
import { connect } from 'react-redux'

import './Header.css'

class Header extends Component {
  renderContent() {
    switch (this.props.auth) {
      case null:
        return <li></li>
      case false:
        return <li></li>
      default:
        return (
          <li key='logout'>
            <a href='/api/logout' className='Header__logout'>
            <svg viewBox="0 0 24 24">
              <path d="M16 9V5l8 7-8 7v-4H8V9h8zM0 2v20h14v-2H2V4h12V2H0z"/>
            </svg>
              Logout
            </a>
          </li>
        )
    }
  }

  render() {
    return (
      <nav className='Header__wrapper'>
        <div>
          <div>
            <ul className='right'>
              {this.renderContent()}
            </ul>
          </div>
        </div>
      </nav>
    )
  }
}

function mapStateToProps({ auth }) {
  return { auth }
}

export default connect(mapStateToProps, null)(Header)
