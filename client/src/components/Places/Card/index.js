import React from 'react'
import PropTypes from 'prop-types'
import './PlaceCard.css'
import Rating from '../Rating'

export default function Card({ data, onClick, index }) {
  const style = {
    animationDelay: index * 0.15 + 's'
  }
  return (
    <div className='PlaceCard' onClick={() => { onClick(data.place_id) }} style={style}>
      <div className='PlaceCard__content'>
        <div className='PlaceCard__header'>
          <img src={data.icon} alt=' '/>
          <div className='PlaceCard__title'>
            <h3>{data.name}</h3>
            <p>{data.vicinity}</p>
          </div>
        </div>
        <div className='PlaceCard__body'>
          <Rating rating={data.customRating} disabled />
        </div>
      </div>
    </div>
  )
}

Card.propTypes = {
  data: PropTypes.object,
}