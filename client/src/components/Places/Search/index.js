import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../../actions'

import './Search.css'

class Search extends Component {
  
  state = {
    value: '',
  }

  lastValue = ''

  onChange = (event) => {
    const value = event.target.value.replace(/\D/g, '').slice(0, 9)
    this.setState({ value })
  }

  onClick = () => {
    if (this.state.value && this.state.value !== this.lastValue) {
      this.props.fetchPlaces(this.state.value)
      this.lastValue = this.state.value
    }
  }

  onKeyDown = (event) => {
    if (event.key === 'Enter') this.onClick()
  }

  render() {
    const disabled = this.props.places.fetching
    return (
      <div className='Search__wrapper'>
        <div className='Search__content'>
          <div className='Search__heading'>
            <h2><span>Biz</span>Rating</h2>
          </div>
          <div className='Search__container'>
            <div className='Search__box' >
              <button className='Search__button' onClick={this.onClick} disabled={disabled}>
                <svg viewBox='0 0 24 24' className='Search__svg'>
                  <path d='M21.172 24l-7.387-7.387C12.397 17.487 10.761 18 9 18c-4.971 0-9-4.029-9-9s4.029-9 9-9 9 4.029 9 9c0 1.761-.514 3.398-1.387 4.785L24 21.172 21.172 24zM9 16c3.859 0 7-3.14 7-7s-3.141-7-7-7-7 3.14-7 7 3.141 7 7 7z'/>
                </svg>
              </button>
              <input
                className='Search__input'
                disabled={disabled}
                type='text'
                placeholder='Search Zipcode'
                onChange={this.onChange}
                onKeyDown={this.onKeyDown}
                value={this.state.value}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps({ places, zipcode }) {
  return { places, zipcode }
}

function mapDispatchToProps(dispatch) {
  return { 
    fetchPlaces: (zipcode) => { dispatch(actions.fetchPlaces(zipcode)) },
    fetchZipcode: () => { dispatch(actions.fetchZipcode()) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)