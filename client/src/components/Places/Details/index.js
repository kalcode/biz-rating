import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as actions from '../../../actions'
import Info from './Info'
import Rating from '../Rating'
import Loading from './Loading'

import './Details.css'

class Details extends Component {
  static propTypes = {
    placeId: PropTypes.string,
  }

  componentDidMount() {
    if (!this.props.details[this.props.placeId]) {
      this.props.fetchDetails(this.props.placeId)
    }
  }

  render() {
    const { details, placeId, onClick } = this.props
    const data = details[placeId]
    if (details.error) return <div dangerouslySetInnerHTML={{ __html: details.error }} />
    if (details.fetching || !data) return <Loading />
    return (
      <div className='Details__wrapper'>
        <div className='Details__container'>
          <div className='Details__header'>
            <div className='Details__icon'>
              <img src={data.icon} alt='' />
            </div>
            <div className='Details__title'>
              <h2>{data.name}</h2>
            </div>
            <div className='Details_close' onClick={() => { onClick(null) }}>
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" className='Details__svg'>
                <path d="M24 20.188l-8.315-8.209 8.2-8.282L20.188 0l-8.212 8.318L3.666.115 0 3.781l8.321 8.24-8.206 8.313L3.781 24l8.237-8.318 8.285 8.203z"/>
              </svg>
            </div>
          </div>
          <div className='Details_body'>
            <Info icon='Location'><p>{data.formatted_address}</p></Info>
            <Info icon='Map'><p><a target='_blank' href={data.url}>Click here for Google Maps</a></p></Info>
            {data.formatted_phone_number && <Info icon='Phone'><p>{data.formatted_phone_number}</p></Info>}
            {data.opening_hours && <Info icon='Clock'>
              <ul className='Details__list'>
                {data.opening_hours.weekday_text.map((hour, key) => (
                  <li key={key}>{hour}</li>
                ))}
              </ul>
            </Info>}
            <div className='Details__rating'>
              <h3>Rating: </h3>
              <Rating rating={data.customRating} placeId={placeId} />
            </div>
          </div>
        </div>
      </div>
    )
  }
}


function mapStateToProps({ details }) {
  return { details }
}

function mapDispatchToProps(dispatch) {
  return { 
    fetchDetails: (placeId) => { dispatch(actions.fetchDetails(placeId)) },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Details)