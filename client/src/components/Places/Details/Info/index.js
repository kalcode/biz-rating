import React from 'react'

import * as SVG from './SVG'
import './Info.css'

export default function Info({ children, icon }) {
  const Icon = SVG[icon]
  return (
    <div className='Info__wrapper'>
      <div className='Info_icon'>
        <Icon />
      </div>
      <div className='Info_content'>
        {children}
      </div>
    </div>
  )
}
