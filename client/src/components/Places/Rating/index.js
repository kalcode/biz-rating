import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as SVG from './SVG'
import { connect } from 'react-redux'
import * as actions from '../../../actions'

import './Rating.css'

class Rating extends Component {
  static propTypes = {
    rating: PropTypes.number,
    placeId: PropTypes.string,
  }

  state = {
    hover: null,
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.rating !== this.props.rating) {
      this.setState({ hover: null })
    }
  }

  onMouseOver = (event) => {
    if (this.props.disabled) return 
    const rating = event.target.dataset && event.target.dataset.rating
    this.setState({ hover: rating })
  }

  onMouseLeave = (event) => {
    if (this.props.disabled) return
    this.setState({ hover: null })
  }

  onClick = (score) => {
    if (this.props.disabled) return
    this.props.ratePlace(score, this.props.placeId)
  }

  getStar(ratingPos) {
    const rating = this.state.hover || this.props.rating || 0
    let star = 'Empty';
    const adjusted = ratingPos - rating
    if (adjusted <= 0.15) star = 'Full'
    else if (adjusted > 0.15 && adjusted < 0.8) star = 'Half'
    const Star = SVG[star]
    return (
      <span onClick={() => { this.onClick(ratingPos) }} data-rating={ratingPos}>
        <Star />
      </span>
    )
  }

  render() {
    let className = 'Rating__container'
    if (this.state.hover) className += ' Rating__hover'
    else if (!this.props.rating) className += ' Rating__none'
    return (
      <div className='Rating__wrapper'>
        <div className={className} onMouseLeave={this.onMouseLeave} onMouseOver={this.onMouseOver}>
          {this.getStar(1)}
          {this.getStar(2)}
          {this.getStar(3)}
          {this.getStar(4)}
          {this.getStar(5)}
          {!this.props.rating && <div className='Rating__note'>
            Not Yet Rated
          </div>}
        </div>
      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return { 
    ratePlace: (score, placeId) => { dispatch(actions.ratePlace(score, placeId)) },
  }
}

export default connect(null, mapDispatchToProps)(Rating)