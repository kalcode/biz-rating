import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Card from './Card'
import Details from './Details'

import './Places.css'

class Places extends Component {
  static propTypes = {
    data: PropTypes.object,
  }

  state = {
    current: null,
  }

  onClick = (placeId) => {
    this.setState({ current: placeId || null })
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.current) {
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.removeProperty('overflow')
      if (!document.body.getAttribute('style')) document.body.removeAttribute('style')
    }
  }

  render() {
    const { places } = this.props
    const { data, error, fetching } = places
    const { current } = this.state
    return (
      <div className='Places__wrapper'>
        {error && <div className='Places__error'><strong>Error:</strong> {error.response.data.status}</div>}
        {fetching && <div className='Places__loading'>Loading...</div>}
        {current && <Details placeId={current} onClick={this.onClick}/>}
        {data && data.results.map((item, key) => (
            <Card key={item.place_id} data={item} onClick={this.onClick} index={key} />
          ))}
      </div>
    )
  }
}

function mapStateToProps({ places }) {
  return { places }
}

export default connect(mapStateToProps, null)(Places)