import React, { Fragment, Component } from 'react'
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import * as actions from '../../actions'

import Header from '../Header'
import Landing from '../../pages/Landing'
import Places from '../../pages/Places'

function AuthRoute({ component: Comp, auth, ...rest }) {
  return (
    <Route {...rest} render={(props) => (
      auth || auth.fetching ? <Comp {...props} /> : <Redirect to='/' />
    )}
    />
  )
}

export class App extends Component {
  componentDidMount() {
    this.props.fetchUser()
  }

  render() {
    return (
      <Fragment>
        <Header />
        <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Landing} />
          <AuthRoute path='/places' auth={this.props.auth} component={Places} />
        </Switch>
        </BrowserRouter>
      </Fragment>
    )
  }
}

function mapStateToProps({ auth }) {
  return { auth }
}

export default connect(mapStateToProps, actions)(App)