import { FETCHED_ZIPCODE } from '../actions/types'

export default function(state = null, action) {
  switch (action.type) {
    case FETCHED_ZIPCODE:
      return action.payload.short_name
    default:
      return state;
  }
}