import { FETCH_USER, FETCHED_USER } from '../actions/types'

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_USER:
      return { fetching: true }
    case FETCHED_USER:
      return action.payload || false
    default:
      return state;
  }
}