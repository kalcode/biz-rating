import { FETCH_DETAILS, FETCHED_DETAILS, UPDATE_RATING } from '../actions/types'

const INITIAL_STATE = {
  fetching: false,
  error: null,
}

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_DETAILS:
      return { ...state, fetching: true }
    case FETCHED_DETAILS:
      if (action.payload.error) {
        return { ...state, fetching: false, error: action.payload.error }
      }
      const result = action.payload.result
      return { ...state, fetching: false, [result.place_id]: result }
    case UPDATE_RATING:
      const data = action.payload
      if (state[data.place_id]) {
        return { ...state, fetching: false, [data.place_id]: { ...state[data.place_id], ...data } }
      }
      return state
    default:
      return state;
  }
}