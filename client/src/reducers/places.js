import { FETCH_PLACES, FETCHED_PLACES, UPDATE_RATING } from '../actions/types'

import filter from './filterTypes.json'

const INITIAL_STATE = {
  fetching: false,
  data: null,
  error: null,
}

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_PLACES:
      return { ...state, data: null, error: null, fetching: true }
    case FETCHED_PLACES:
      if (action.payload.error) {
        return {...state, fetching: false, error: action.payload.error }
      }
      if (action.payload.results) {
        // filter out non Type 1 results
        // https://developers.google.com/places/supported_types
        action.payload.results = action.payload.results.filter(result => {
          return result.types.some(type => filter.list.indexOf(type) > -1)
        })
      }
      return { ...state, fetching: false, data: action.payload }
    case UPDATE_RATING:
      const indexPlaceId = state.data.results.findIndex(place => place.place_id === action.payload.place_id)
      if (indexPlaceId > -1) {
        const cloneState = { ...{}, ...state }
        cloneState.data.results[indexPlaceId] = { ...cloneState.data.results[indexPlaceId],  ...action.payload}
        return cloneState
      }
      return state
    default:
      return state;
  }
}