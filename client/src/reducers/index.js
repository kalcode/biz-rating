import { combineReducers } from 'redux'
import auth from './auth'
import places from './places'
import zipcode from './zipcode'
import details from './details'

export default combineReducers({
  auth,
  places,
  zipcode,
  details,
})