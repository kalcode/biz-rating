import React from 'react'
import PlacesManager from '../components/Places'
import Search from '../components/Places/Search'

export default function PlacesPage() {
  return (
    <main>
      <Search />
      <PlacesManager />
    </main>
  )
}