import React from 'react'
import './Landing.css'

import SignInButton from '../components/SignIn'

const Landing = () => (
  <main>
    <div className='Landing__wrapper'>
      <div className='Landing__header'>
        <h2><span>Biz</span>Rating</h2>
        <p>
          See and rate businesses around you.<br />
          An innovative and unique approach to finding establishments near you.
        </p>
        <div className='Landing__signin'>
          <SignInButton />
        </div>
      </div>
      <div className='Landing__content'>
        <div className='Landing__row'>
          <div className='Landing__col'>
            <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' className='Landing__svg'>
              <path d='M22.351 12L24 9.916l-2.272-1.403L22.553 6l-2.624-.553-.098-2.64-2.655.364L16.167.723l-2.37 1.237L12 0l-1.797 1.961L7.833.724 6.824 3.171l-2.655-.363-.098 2.64L1.447 6l.825 2.513L0 9.916 1.649 12 0 14.084l2.272 1.403L1.447 18l2.624.553.098 2.64 2.655-.364 1.009 2.448 2.37-1.237L12 24l1.797-1.96 2.37 1.237 1.009-2.448 2.655.364.098-2.64L22.553 18l-.825-2.513L24 14.084 22.351 12zm-6.043-.206c.418.056.63.328.63.61 0 .323-.277.66-.844.705-.348.027-.434.312-.016.406.351.08.549.326.549.591 0 .314-.279.654-.913.771-.383.07-.421.445-.016.477.344.026.479.146.479.312 0 .466-.826 1.333-2.426 1.333C11.25 17 10.344 15.5 7 15.5v-4.964c1.766-.271 3.484-.817 4.344-3.802.239-.831.39-1.734 1.187-1.734 1.188 0 1.297 2.562.844 4.391.656.344 1.875.468 2.489.442.886-.036 1.136.409 1.136.745 0 .505-.416.675-.677.755-.304.094-.444.404-.015.461z'/>
            </svg>
          </div>
          <div className='Landing__col'>
            <h3>Features: Ratings</h3>
            <p>
              We know finding businesses is easy, but finding a rating for them? Impossible. Here we provide you a simple way to search and start rating businesses around you.
            </p>
          </div>
        </div>
        <div className='Landing__row'>
          <div className='Landing__col Landing__reverse'>
            <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' className='Landing__svg'>
              <path d='M12 0C5.373 0 0 5.373 0 12s5.373 12 12 12 12-5.373 12-12S18.627 0 12 0zm1 16.947V18h-1v-.998c-1.035-.018-2.106-.265-3-.727l.455-1.644c.956.371 2.229.765 3.225.54 1.149-.26 1.384-1.442.114-2.011-.931-.434-3.778-.805-3.778-3.243 0-1.363 1.039-2.583 2.984-2.85V6h1v1.018c.724.019 1.536.145 2.442.42l-.362 1.647c-.768-.27-1.617-.515-2.443-.465-1.489.087-1.62 1.376-.581 1.916 1.712.805 3.944 1.402 3.944 3.547.002 1.718-1.343 2.632-3 2.864z'/>
            </svg>
          </div>
          <div className='Landing__col Landing__text-reverse'>
            <h3>Cost: Free</h3>
            <p>
              That's right, free. We also don't share your infomation to monetize and that is a Zuckerberg promise. 
            </p>
          </div>
        </div>
        <div className='Landing__row'>
          <div className='Landing__col'>
            <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' className='Landing__svg'>
              <path d='M16.625 8.292c0 .506-.41.917-.917.917s-.916-.411-.916-.917.409-.917.916-.917.917.411.917.917zM24 12c0 6.627-5.373 12-12 12S0 18.627 0 12 5.373 0 12 0s12 5.373 12 12zm-11.293 1.946c-1.142-.436-2.065-1.312-2.561-2.423L7 14.708V17h3v-1h1v-1h.672l1.035-1.054zM18 9.667C18 7.642 16.358 6 14.333 6c-2.024 0-3.666 1.642-3.666 3.667s1.642 3.667 3.666 3.667C16.358 13.333 18 11.691 18 9.667z'/>
            </svg>
          </div>
          <div className='Landing__col'>
            <h3>SignIn: Easy and Secure</h3>
            <p>
              Sign In with Google makes it easy and secure to get started with us. Not only that, we also use blockchains and cryptocurrency cause its so <span role='img' aria-label='hot'>🔥</span> right now. <br />
              <span style={{ fontSize: '0.65em', lineHeight: '3' }}>*disclaimer: we don't use any blockchains</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  </main>
)

export default Landing