require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const cookiesSession = require('cookie-session')
const passport = require('passport')
const bodyParser = require('body-parser')

// Database && Auth
require('./models/User')
require('./models/Place')
require('./services/passport')

const DB_USER = process.env.DB_USER
const DB_PASS = process.env.DB_PASS
const DB_HOST = process.env.DB_HOST
const mongoURI = `mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}`
mongoose.connect(mongoURI)

// App Setup
const app = express()

const cookieOptions = {
  maxAge: 30 * 24 * 60 * 60 * 1000,
  keys: [process.env.COOKIE_KEY],
}

app.use(bodyParser.json())
app.use(cookiesSession(cookieOptions))

// Passport
app.use(passport.initialize())
app.use(passport.session())

// Routing
require('./routes/auth')(app)
require('./routes/googlePlaces')(app)
require('./routes/ratings')(app)

if (process.env.NODE_ENV === 'production') {
  // Serve static assets
  app.use(express.static('client/build'))

  // Serve index.html for non routed paths
  const path = require('path')
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
  })
}

const PORT  = process.env.PORT || 5000
app.listen(PORT, () => {
  console.log('\n\x1b[33mServer running on:\x1b[34m http://localhost:' + PORT + '\x1b[0m \n')
})