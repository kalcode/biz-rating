const mongoose = require('mongoose')
const { Schema } = mongoose
const VoteSchema = require('./Vote')

const schema = new Schema({
  placeId: { type: String, index: true },
  votes: [VoteSchema],
  rating: Number,
})

// calculate the new rating before being saved
schema.pre('save', function(next){
  let sum = 0
  this.votes.forEach(vote => { sum += vote.score })
  this.rating = sum / this.votes.length
  next()
})

mongoose.model('places', schema)