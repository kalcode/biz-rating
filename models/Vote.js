const mongoose = require('mongoose')
const { Schema } = mongoose

const schema = new Schema({
  score: Number,
  _user: { type: Schema.Types.ObjectId, ref: 'User' },
})

module.exports = schema