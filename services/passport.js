const passport = require('passport')
const GoogleStrategy = require('passport-google-oauth20').Strategy
const mongoose = require('mongoose')

const User = mongoose.model('users')

passport.serializeUser((user, done) => {
  done(null, user.id)
})

passport.deserializeUser((id, done) => {
  User.findById(id)
    .then(user => done(null, user), err => console.log(err))
})

options = {
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: '/auth/google/callback',
  proxy: true,
}

const googleStrategy = new GoogleStrategy(
  options,
  async (accessToken, refreshToken, profile, done) => {
    // see if user exists
    const existingUser = await User.findOne({ googleId: profile.id }) 

    if (existingUser) return done(null, existingUser)

    // create new user and save
    const user = await new User({ googleId: profile.id }).save()
    done(null, user)
  })

passport.use(googleStrategy)

// Example structure Google API + returns
// {
//   id: string, 
//   name: { familyName: string, givenName: string },
//   emails: [{ value: string, type: string }],
//   photos: [ { value: string }],
//   provider: 'google',
// }