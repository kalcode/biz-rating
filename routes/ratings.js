const mongoose = require('mongoose')

const isAuthenticated = require('../middlewares/isAuthenticated')

const Place = mongoose.model('places')

module.exports = (app) => {
  app.post('/api/rate', isAuthenticated, async (req, res) => {
    // grab zipcode from url params
    const score = req.body.score
    const placeId = req.body.placeid
    const user = req.user
    let place = await Place.findOne({ placeId })
    if (!place) {
       place = new Place({
         placeId,
         rating: 0,
         votes: [{ score, _user: user.id}],
       })
    } else  {
      const index = place.votes.findIndex(vote => {
        return String(vote._user) === String(user._id)
      })
      if (index > -1) {
        place.votes[index].score = score
      } else {
        place.votes.push({ score, _user: user })
      }
    }
    upDatedPlace = await place.save()
    data = { customRating: upDatedPlace.rating, place_id: upDatedPlace.placeId }
    res.json(data)
  })

}
// Post request example
// fetch('/api/rate', {
// 	method: 'POST',
// 	credentials: 'same-origin',
// 	headers: { 'Content-Type': 'application/json' },
// 	body: JSON.stringify({score: 1, placeid: 'ChIJI3E8ce41R4YRKL5V9FiLVk0' })
// }).then(res => res.json()).then(json => console.log(json))