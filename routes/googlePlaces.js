const mongoose = require('mongoose')
const fetch = require('node-fetch')
const isAuthenticated = require('../middlewares/isAuthenticated')

const key = process.env.GOOGLE_API_KEY

const Place = mongoose.model('places')

module.exports = (app) => {
  app.get('/api/place/search', isAuthenticated, async (req, res) => {
    // grab zipcode from url params
    const zipcode = req.query.zipcode
    if (!zipcode) return res.status(400).json({ error: 'Zipcode query required', status: 'BAD_REQUEST' })

    // transform zipcode into location object (long, lat)
    const geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${zipcode}&key=${key}`
    const googleGeoData = await fetch(geocodeUrl).then(res => res.json())

    if (googleGeoData.status !== 'OK') return res.status(400).json(googleGeoData)
    // get lat and long from geocoding data
    const location = googleGeoData.results[0].geometry.location
    const { lat , lng } = location

    const radius = req.query.radius || 5000

    // request places around zipcode
    const googlePlacesUrl = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${lat},${lng}&rankby=distance&key=${key}`
    const googlePlacesData = await fetch(googlePlacesUrl).then(res => res.json())

    if (googlePlacesData.status !== 'OK') return res.status(400).json(googlePlacesData)

    // fetch places rating stored
    const customPlaces = await getMultiplePlaces(googlePlacesData)

    // merge custom ratings
    googlePlacesData.results.forEach(place => {
      const customRating = customPlaces.find(customPlace => customPlace.placeId === place.place_id)
      if (customRating) place.customRating = customRating.rating
    })

    return res.json(googlePlacesData)
  })

  app.get('/api/place/details', isAuthenticated, async (req, res) => {
    // grab zipcode from url params
    const placeid = req.query.placeid
    if (!placeid) return res.status(400).json({ error: 'Placeid query required', status: 'BAD_REQUEST' })

    // transform zipcode into location object (long, lat)
    const placeDetail = `https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeid}&key=${key}`
    let data = null
    try {
      data = await fetch(placeDetail).then(res => res.json())
    } catch(e) {
      data = e
    }

    if(data.status !== 'OK') return res.status(400).json({ error: data.status })
    data.result.customRating = await getPlaceRating(placeid)
    return res.json(data)
  })

  app.get('/api/place/zipcode', isAuthenticated, async (req, res) => {
    
    // grab zipcode from url params
    const latlng = req.query.latlng
    
    if (!latlng) return res.status(400).json({ error_message: 'latlng query required', status: 'BAD_REQUEST' })

    // transform zipcode into location object (long, lat)
    const geocode = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latlng}&key=${key}`
    const data = await fetch(geocode).then(res => res.json())

    if(data.status !== 'OK') return res.status(400).json(data)
    zipcode = data.results[0].address_components.find((item) => item.types[0] === 'postal_code')

    return res.json(zipcode)
    })
}

function getPlaceRating(placeId) {
  return new Promise(async resolve => {
    const place = await Place.findOne({ placeId }) 
    if (place) resolve(place.rating)
    resolve(null)
  })
}

function getMultiplePlaces(places) {
  return new Promise(async resolve => {
    const placesIdArray = places.results.map(place => place.place_id)
    const customPlaces = await Place.find({ placeId: { $in: placesIdArray }})
    resolve(customPlaces)
  })
}